import java.util.List;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

class FaceDetection {


        public void detectFace(Mat frame, CascadeClassifier faceCascade) {
            Mat frameGray = new Mat();
            Imgproc.cvtColor(frame, frameGray, Imgproc.COLOR_BayerGR2RGB);
            Imgproc.equalizeHist(frameGray, frameGray);
            // Detect faces
            MatOfRect faces = new MatOfRect();
            faceCascade.detectMultiScale(frameGray, faces);
            List<Rect> listOfFaces = faces.toList();
            for (Rect face : listOfFaces) {
                Point center = new Point(face.x + face.width / 2, face.y + face.height / 2);
                Imgproc.ellipse(frame, center, new Size(face.width / 2, face.height / 2), 0, 0, 360,
                        new Scalar(255, 0, 255));
                Mat faceROI = frameGray.submat(face);
            }
            //Display Face Detection
            HighGui.imshow("Face detection", frame );
        }
        public void run(String[] args) {
            /*String filenameFaceCascade = args.length > 2 ? args[0] : "haarcascades/haarcascade_frontalface_alt.xml";
            String filenameEyesCascade = args.length > 2 ? args[1] : "haarcascades/haarcascade_eye_tree_eyeglasses.xml";

            CascadeClassifier faceCascade = new CascadeClassifier();
            CascadeClassifier eyesCascade = new CascadeClassifier();
            if (!faceCascade.load(filenameFaceCascade)) {
                System.err.println("Error Face " + filenameFaceCascade);
                System.exit(0);
            }
            if (!eyesCascade.load(filenameEyesCascade)) {
                System.err.println("Error Eyes " + filenameEyesCascade);
                System.exit(0);
            }*/
            String imgFile = "images/test.png";
            Mat src = Imgcodecs.imread(imgFile);

            CascadeClassifier cc = new CascadeClassifier("haarcascades/haarcascade_frontalface_alt.xml");

            MatOfRect faceDetection = new MatOfRect();
            cc.detectMultiScale(src, faceDetection);
            System.out.println(String.format("Detected faces: %d", faceDetection.toArray().length));


            //test
            /*

            String imgFile = "images/test.png";
            Mat src = Imgcodecs.imread(imgFile);

            CascadeClassifier cc = new CascadeClassifier("haarcascades/haarcascade_frontalface_alt.xml");

            MatOfRect faceDetection = new MatOfRect();
            cc.detectMultiScale(src, faceDetection);
            System.out.println(String.format("Detected faces: %d", faceDetection.toArray().length));

            for(Rect rect: faceDetection.toArray()) {
                Imgproc.rectangle(src, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height) , new Scalar(0, 0, 255), 3);
            }

            Imgcodecs.imwrite("images/test_out.png", src);
            System.out.println("Image Detection Finished");

*/
            //Create Video Capture Instance
            int cameraDevice = args.length > 2 ? Integer.parseInt(args[2]) : 0;
            VideoCapture capture = new VideoCapture(cameraDevice);
            if (!capture.isOpened()) {
                System.err.println("Error: Video");
                System.exit(0);
            }
            Mat frame = new Mat();
            while (capture.read(frame)) {
                if (frame.empty()) {
                    System.err.println("No Video Frame Detected");
                    break;
                }
                //-- 3. Apply the classifier to the frame
                detectFace(frame, cc);
                if (HighGui.waitKey(10) == 27) {
                    break;// escape
                }
            }


            System.exit(0);
        }

}
