import org.opencv.core.Core;

public class Program {

    public static void main(String[] args) {
        //s Load the native OpenCV library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
       new FaceDetection().run(args);
        //FaceDetectionBase f = new FaceDetectionBase();
        //f.faceInPicture();

    }
}
